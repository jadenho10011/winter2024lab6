public class LuckyCardGameApp {
    public static void main(String[] args) {
        GameManager manager = new GameManager();
        int totalPoints = 0;
        int rounds = 0;

        System.out.println("Welcome to the Lucky Card Game!");

        while (manager.getNumberOfCards() > 1 && totalPoints < 5) {
            rounds++;

            System.out.println("Round " + rounds);
            System.out.println(manager);

            totalPoints += manager.calculatePoints();
            System.out.println("Total points: " + totalPoints);

            manager.dealCards();
        }

        System.out.println("Final score: " + totalPoints);
        if (totalPoints < 5) {
            System.out.println("You lose the game!");
        } else {
            System.out.println("Congratulations! You win the game!");
        }
    }
}
