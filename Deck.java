import java.util.Random;

public class Deck{
	private Card[] cards;
	private int numberOfCards;
	private Random rng;
	
	public Deck(){
		rng = new Random();
		cards = new Card[52];
		numberOfCards = 52;
		
		String[] suits = {"Hearts", "Diamonds", "Clubs", "Spades"};
		String[] values = {"Ace", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Jack", "Queen", "King"};
		
		int i = 0;
		for(String suit : suits){
			for(String value : values){
				cards[i] = new Card(suit, value);
				i++;
			}
		}
	}
	
	public int length(){
		return numberOfCards;
	}
	
	public Card drawTopCard(){
		if(numberOfCards == 0){
			return null;
		}
		numberOfCards--;
		return cards[numberOfCards];
	}
	
	public String toString(){
		String Builder = "";
		for(int i = 0; i < numberOfCards; i++){
			Builder += cards[i] + "\n";
		}
		return Builder;
	}
	
	public void shuffle() {
        for (int i = 0; i < numberOfCards; i++) {
            int randomIndex = rng.nextInt(numberOfCards);
            Card temp = cards[i];
            cards[i] = cards[randomIndex];
            cards[randomIndex] = temp;
        }
    }
}