public class GameManager {
    private Deck drawPile;
    private Card centerCard;
    private Card playerCard;

    public GameManager() {
        drawPile = new Deck();
        drawPile.shuffle();

        centerCard = drawPile.drawTopCard();
        playerCard = drawPile.drawTopCard();
    }

	public String toString() {
		String builder = "";
		builder += "-------------------------------------------------------------\n";
		builder += "Center card: " + centerCard + "\n";
		builder += "Player card: " + playerCard + "\n";
		builder += "-------------------------------------------------------------\n";
		return builder;
	}

    public void dealCards() {
        drawPile.shuffle();

        centerCard = drawPile.drawTopCard();
        playerCard = drawPile.drawTopCard();
    }

    public int getNumberOfCards() {
        return drawPile.length();
    }

    public int calculatePoints() {
        if (centerCard.getValue().equals(playerCard.getValue())) {
            return 4; 
        } else if (centerCard.getSuit().equals(playerCard.getSuit())) {
            return 2; 
        } else {
            return -1; 
        }
    }
}
